package ru.csm.api.network;

public class Channels {

    public static final String SKINS_APPLY = "csm:skins_apply";
    public static final String SKINS_REFRESH = "csm:skins_refresh";

    public static final String SKINS_PLAYER = "csm:skins_player";
    public static final String SKINS_URL = "csm:skins_url";
    public static final String SKINS_RESET = "csm:skins_reset";
    public static final String SKINS_MENU = "csm:skins_menu";

    public static final String SKINS_CITIZENS = "csm:skins_citizens";
}
